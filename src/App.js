import "./App.css";
import Navbar from "./components/Navbar";
import headerImg from "../src/images/benjamin-wong-4-ka5rdCesI-unsplash 1.png";
import { Box, Typography } from "@mui/material";
import WebFont from "webfontloader";
import { useEffect } from "react";
import CompanyDesc from "./components/CompanyDesc";
import CoreValues from "./components/CoreValues";
import Perks from './components/Perks';

function App() {
	useEffect(() => {
		WebFont.load({
			google: {
				families: ["Rubik"],
			},
		});
	}, []);

	const titleStyle = {
		fontSize: "3rem",
		fontWeight: 700,
		fontFamily: "Rubik",
		lineHeight: "1em",
		mb: "2vh",
	};

	const subtitleStyle = {
		fontSize: "1.5rem",
		fontFamily: "Rubik",
		lineHeight: "1em",
		mb: "2vh",
	};

	const contentStyle = {
		fontSize: "1.2rem",
		fontFamily: "Rubik",
		lineHeight: "1.5em",
		mb: "2vh",
	};

	return (
		<div className="App">
			<Navbar />
			<Box className="App-section" sx={{ background: "#4097DB" }}>
				<img src={headerImg} style={{ width: "100%" }} alt="headerImg"/>
				<Box
					sx={{
						flexDirection: "column",
						paddingLeft: "5vh",
						paddingRight: "5vh",
						background: "#4097DB",
						height: "50vh",
					}}
					className="App-section"
				>
					<Typography sx={titleStyle} style={{ color: "#FFF" }}>
						Discover Your Wonder
					</Typography>
					<Typography sx={contentStyle} style={{ color: "#FFF" }}>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit,
						sed do eiusmod tempor incididunt ut labore et dolore
						magna aliqua. Ut enim ad minim veniam.
					</Typography>
				</Box>
			</Box>
			<CompanyDesc
				titleStyle={titleStyle}
				subtitleStyle={subtitleStyle}
				contentStyle={contentStyle}
			/>
            <CoreValues 
                titleStyle={titleStyle}
                subtitleStyle={subtitleStyle}
                contentStyle={contentStyle}
            />
            <Perks 
                titleStyle={titleStyle}
                subtitleStyle={subtitleStyle}
                contentStyle={contentStyle}
            />
		</div>
	);
}

export default App;
