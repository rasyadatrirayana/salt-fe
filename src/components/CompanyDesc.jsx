import React, { useEffect, useState } from "react";
import { Box, Button, Stack, Typography } from "@mui/material";
import "../App.css";
import WestIcon from "@mui/icons-material/West";
import EastIcon from "@mui/icons-material/East";

const CompanyDesc = (props) => {

	const [ page, setPage ] = useState(0);
    const [ isNextDisabled, setIsNextDisabled ] = useState(false);
    const [ isPrevDisabled, setIsPrevDisabled ] = useState(false);

	function createData(title, subtitle, content) {
		return { title, subtitle, content };
	}

	const pages = [
		createData(
			"Who we are",
			"Technology Company",
			"Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo."
		),
		createData(
			"What we do",
			"Professional Brand Management",
			"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."
		),
		createData(
			"How we do",
			"Strategize, Design, Collaborate",
			"Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse sequam nihil molestiae consequatur."
		),
	];

    function handleNext(){
        setPage(page + 1)
    }
    
    function handlePrev(){
        setPage(page - 1)
    }

    function handleNextDisabled(){

        if(page >= (pages.length - 1)){
            setIsNextDisabled(true)
        }
        else{
            setIsNextDisabled(false)
        }

    }

    function handlePrevDisabled(){

        if(page <= 0){
            setIsPrevDisabled(true)
        }
        else{
            setIsPrevDisabled(false)
        }

    }

    useEffect(() => {

        handleNextDisabled()
        handlePrevDisabled()

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [page])
    

    // function 

	return (
        <Box
            sx={{
                bgcolor: "#FFF",
                height: "30rem",
                flexDirection: "column",
                paddingLeft: "5vh",
                paddingRight: "5vh",
            }}
            className="App-section"
        >
            <Box>
                <Typography sx={props.titleStyle} style={{ color: "#029FE4" }}>
                    {pages[page].title}
                </Typography>
                <Typography sx={props.subtitleStyle} style={{ color: "#000" }}>
                    {pages[page].subtitle}
                </Typography>
                <Typography sx={props.contentStyle} style={{ color: "#777" }}>
                    {pages[page].content}
                </Typography>
            </Box>
            <Box>
                <Stack direction="row-reverse" sx={{ marginTop: "2rem" }} >
                    <Button
                        variant="contained"
                        sx={{
                            background: "#1BA0E1",
                            borderRadius: 0,
                            height: "3.5rem",
                            width: "3.5rem",
                        }}
                        onClick={handleNext}
                        disabled={isNextDisabled}
                    >
                        <EastIcon />
                    </Button>
                    <Button
                        variant="contained"
                        sx={{
                            background: "#1BA0E1",
                            borderRadius: 0,
                            height: "3.5rem",
                            width: "3.5rem",
                        }}
                        onClick={handlePrev}
                        disabled={isPrevDisabled}
                    >
                        <WestIcon />
                    </Button>
                </Stack>
            </Box>
        </Box>
	);
};

export default CompanyDesc;
