import { AppBar, Box, IconButton, Toolbar } from '@mui/material'
import React from 'react'
import company_logo from '../images/company_logo.png'
import MenuIcon from '@mui/icons-material/Menu';

const Navbar = () => {
  return (
    <Box>
        <AppBar position='static' style={{ background: '#FFFFFF' }}>
            <Toolbar>
                <Box 
                    sx={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        p: 1,
                        m: 1,
                    }}  
                >
                    <img src={company_logo} style={{ height: '100%' }} alt='logo'/>
                    <IconButton>
                        <MenuIcon />
                    </IconButton>
                </Box>
            </Toolbar>
        </AppBar>
    </Box>
  )
}

export default Navbar