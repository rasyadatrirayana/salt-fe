import { Box, Typography } from "@mui/material";
import React, { useState } from "react";
import accImg from "../images/Accesories.png"
import speedImpImg from "../images/Speed Improvement.png"
import exhaustImg from "../images/Exhaust.png"

const Perks = (props) => {

	const [ page, setPage ] = useState(0)

	function createData(image, subtitle, content) {
		return { image, subtitle, content };
	}

	const datas = [
		createData(
			accImg,
			"Dedication",
			"Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat."
		),
		createData(
			speedImpImg,
			"Intellectual Honesty",
			"Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias conse."
		),
		createData(
			exhaustImg,
			"Curiosity",
			"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque."
		),
	];

	return (
		<Box
			sx={{
				paddingTop: "2rem",
				paddingBottom: "3rem",
				paddingLeft: "1rem",
				paddingRight: "1rem",
				background: "#509FDD",
			}}
		>
			<Box
				sx={{
					paddingTop: "3rem",
					paddingBottom: "3rem",
					paddingLeft: "2rem",
					paddingRight: "2rem",
					background: "#FFF",
				}}
			>
				<Typography
					sx={props.titleStyle}
					style={{ color: "#029FE4", fontSize: "2rem" }}
				>
					Our Speciality
				</Typography>
				<Typography
					sx={props.content}
					style={{ color: "#303030" }}
				>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					Duis euismod libero vel leo auctor, in venenatis nulla
					consequat. Sed commodo nunc sit amet congue aliquam.
				</Typography>
			</Box>
		</Box>
	);
};

export default Perks;
