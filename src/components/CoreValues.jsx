import { Box, Stack } from "@mui/material";
import React from "react";
import { Typography } from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import coreValueImg from "../images/Core Values Illustration.png"

const CoreValues = (props) => {
    
	function createData(subtitle, content) {
		return { subtitle, content };
	}

	const datas = [
		createData(
			"Dedication",
			"Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat."
		),
		createData(
			"Intellectual Honesty",
			"Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias conse."
		),
		createData(
			"Curiosity",
			"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque."
		),
	];

	return (
        <Box
            sx={{
                background: "#F8F8F8",
                paddingTop: "5rem",
                paddingLeft: "2rem",
                paddingRight: "2rem",
            }}
        >
            <Typography
                sx={props.titleStyle}
                style={{ textAlign: "center", color: "#029FE4" }}
            >
                Our Core Values
            </Typography>
            <Typography sx={props.contentStyle} style={{ color: "#777" }}>
                Ridiculus laoreet libero pretium et, sit vel elementum
                convallis fames. Sit suspendisse etiam eget egestas. Aliquet
                odio et lectus etiam sit.
            </Typography>
            <Typography sx={props.contentStyle} style={{ color: "#777" }}>
                In mauris rutrum ac ut volutpat, ornare nibh diam. Montes,
                vitae, nec amet enim.
            </Typography>
            <Box sx={{ mt: "4rem", mb: "2rem" }}>
                {datas.map((data, index) => (
                    <Stack direction="row">
                        <RemoveIcon/>
                        <Stack direction="column">
                            <Typography
                                sx={props.subtitleStyle}
                                style={{ color: "#000000" }}
                            >
                                {data.subtitle}
                            </Typography>
                            <Typography
                                sx={props.contentStyle}
                                style={{ color: "#777" }}
                            >
                                {data.content}
                            </Typography>
                        </Stack>
                    </Stack>
                ))}
            </Box>
            <img src={coreValueImg} style={{ width: "100%" }} alt=""/>
        </Box>
	);
};

export default CoreValues;
